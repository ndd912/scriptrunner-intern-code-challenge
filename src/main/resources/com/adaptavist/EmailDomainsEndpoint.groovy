package com.adaptavist
import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import groovy.json.JsonBuilder
import groovy.transform.BaseScript

import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response

@BaseScript CustomEndpointDelegate delegate

findDistinctEmailDomains(httpMethod: "GET", groups: ["jira-administrators"]) { MultivaluedMap queryParams, String body ->
    def emailList = ["Gmail.com", "yahoo.com", "aol.com"]
    return Response.ok(new JsonBuilder([abc: emailList]).toString()).build();
}
